class BaseException(Exception):

    message = "An unknown exception occurred."

    def __init__(self, message=None, **kwargs):
        if not message:
            try:
                message = self.message % kwargs
            except Exception:
                message = self.message
        self.msg = message
        super(BaseException, self).__init__(message)

    def __str__(self):
        return self.msg


class NotDefinedDataType(BaseException):
    message = "%(type)s: Data type is not defined."


class PortInstanceCreationError(BaseException):
    message = "module_uuid: %(module_uuid)s, chassis_uuid: %(chassis_uuid)s. \
        One of module_uuid or chassis_uuid must be provided."


class NotSupportedDbClient(BaseException):
    message = "%(db_client_type)s is not supported for DB Client."


class DuplicateCreateOperation(BaseException):
    message = "Create operation failed due to duplication with same UUID."


class NotSupportedDataClass(BaseException):
    message = "%(data_type)s is not supported as Data Type."


class NotYetCreatedDataClass(BaseException):
    message = "%(type)s %(uuid)s is not created yet. Create before updating."


class AlreadyDeletedDataClass(BaseException):
    message = "%(type)s %(uuid)s is already deleted."


class NotSupportedOperation(BaseException):
    message = "%(type)s %(method)s is not supported."


# TODO(yoshito-ito): This class should be more general like:
# DataNotFound(BaseException)
class PortNotFound(BaseException):
    message = "Port %(uuid)s was not found."


# TODO(yoshito-ito): This class should be more general like:
# DataNotFound(BaseException)
class ChassisNotFound(BaseException):
    message = "Chassis %(uuid)s was not found"


# TODO(yoshito-ito): This class should be more general like:
# DataNotFound(BaseException)
class SocketNotFound(BaseException):
    message = "Socket %(uuid)s was not found"
