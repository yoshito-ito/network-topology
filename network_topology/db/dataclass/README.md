# Data Model for Network Topology

A list of possible data models by
[Reo Fujimaki](https://core.ac.uk/download/pdf/156904561.pdf) in May 2018:

1. Physical Topology MIB
2. NDLs (Network Description Languages)
    * NDL (Network Description Language)
    * NML (Network Markup Language)
    * VXDL (Virtual Resources and Interconnection Network Description Language)
    * INDL (Infrastructure and Network Description Language)
3. YANG data model for Network Topology
4. NetJSON

https://library.naist.jp/mylimedio/dllimedio/showpdf2.cgi/DLPDFR008266_P1-94

## Requirements

* To describe "Physical Network Topology"
    * Location: Building, Floor, Rack, Unit
    * Link: Node, Chassis, Slot, Module, Port

* To describe "Logical Network Topology"
    * ????

* Open source schema files available

## Evaluation

### Summary

### 1. Physical Topology MIB

https://tools.ietf.org/html/rfc2922

* Abstract
    This memo defines a portion of the Management Information Base (MIB)
    for use with network management protocols in the Internet community.
    In particular, it describes managed objects used for managing
    physical topology identification and discovery.

* Published: September 2000
* Source code: None
* Last update: None

Other links:
* https://datatracker.ietf.org/doc/rfc2922/
* https://tex2e.github.io/rfc-translater/html/rfc2922.html

### 2. NDLs (Network Description Languages)

* NDL (Network Description Language)
* NML (Network Markup Language)
* VXDL (Virtual Resources and Interconnection Network Description Language)
* INDL (Infrastructure and Network Description Language)

#### NDL (Network Description Language)

https://ieeexplore.ieee.org/document/4258536

* Abstract
    The network description language (NDL) is a vocabulary designed to
    describe optical networks based on the resource description framework
    (RDF). These descriptions aid applications in querying the capabilities
    of the network and allow them to clearly express requests to the network.
    This article introduces NDL and shows its current applications in optical
    research networks.

* Published: May 2007
* Source code: None
* Last update: None

Other links:
* https://www.researchgate.net/publication/4256699_Using_the_Network_Description_Language_in_Optical_Networks
* https://delaat.net/posters/pdf/2006-11-11-ndl.pdf
* https://www.ogf.org/OGF20/materials/727/NDL-Introduction-JvdHam.pdf

#### NML (Network Markup Language)

https://www.ogf.org/documents/GFD.206.pdf

* Abstract
    The Network Markup Language is designed to create a functional description
    of multilayer networks and multi-domain networks. An example of a
    multi-layered network can be a virtualised network, but also using
    different technologies. The multi-domain network descriptions can include
    aggregated or abstracted network topologies. NML can not only describe a
    primarily static network topology, but also its potential capabilities
    (services) and its configuration.

* Published: May 2013
* Source code: None
* Last update: None

Other links:
* https://tnc2013.terena.org/getfile/148

#### VXDL (Virtual Resources and Interconnection Network Description Language)

https://eudl.eu/doi/10.1007/978-3-642-02080-3_15

* Abstract
    This work presents VXDL, a language for virtual resources interconnection
    networks specification and modeling. Besides allowing end resources
    description, VXDL lets users describe the desirable virtual network
    topology, including virtual routers and timeline. In this paper we
    motivate and present the key features of our modeling language.

* Published: May 2012
* Source code: None
* Last update: None

Other links:
* https://eudl.eu/pdf/10.1007/978-3-642-02080-3_15

#### INDL (Infrastructure and Network Description Language)

https://staff.fnwi.uva.nl/p.grosso/INDL.html

* Abstract
    We have extended the ideas of NML also to other kinds of resources,
    as expressed in the figure below. More on this can be found in our paper
    on the Infrastructure & Network Description Language (INDL).  

* Published: July 2012
* Source code: https://github.com/jeroenh/indl
* Last update: 8 years ago

Other links:
* https://ieeexplore.ieee.org/document/6280294
* https://jvdham.nl/research/publications/1204-INDL.pdf

### 3. YANG data model for Network Topology

https://tools.ietf.org/html/rfc8345

* Abstract
    This document defines an abstract (generic, or base) YANG data model
    for network/service topologies and inventories.  The data model
    serves as a base model that is augmented with technology-specific
    details in other, more specific topology and inventory data models.

* Published: March 2018
* Source code: None
* Last update: None

### 4. NetJSON

https://netjson.org/

* Abstract
    NetJSON is a data interchange format based on JavaScript Object Notation
    (JSON) designed to describe the basic building blocks of layer2 and layer3
    networking. It defines several types of JSON objects and the manner in
    which they are combined to represent a network: configuration of devices,
    monitoring data, network topology and routing information.

* Published: July 2015
* Source code: https://github.com/netjson/netjson
* Last update: 6 months ago

Other links:
* https://netjson.org/rfc.html

## References