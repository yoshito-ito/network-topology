from abc import ABC, abstractmethod
import datetime
import json
import uuid

import network_topology.conf as conf
import network_topology.db.client.file as file_client
import network_topology.db.client.etcd as etcd_client
import network_topology.db.dataclass.netjson.device_configuration as device_configuration
import network_topology.db.dataclass.netjson.device_monitoring as device_monitoring
import network_topology.db.dataclass.netjson.network_collection as network_collection
import network_topology.db.dataclass.netjson.network_graph as network_graph
import network_topology.db.dataclass.netjson.network_routes as network_routes
import network_topology.exception as exception


CONF = conf.conf()

DATA_TYPE = ('DeviceConfiguration',
             'DeviceMonitoring',
             'NetworkCollection',
             'NetworkGraph',
             'NetworkRoutes')


def create_dataclass_instance(json_str):
    json_dict = json.loads(json_str)
    data_type = json_dict.get('type')
    if data_type == 'DeviceConfiguration':
        return DeviceConfiguration(json_dict)
    elif data_type == 'DeviceMonitoring':
        return DeviceMonitoring(json_dict)
    elif data_type == 'NetworkCollection':
        return NetworkCollection(json_dict)
    elif data_type == 'NetworkGraph':
        return NetworkGraph(json_dict)
    elif data_type == 'NetworkRoutes':
        return NetworkRoutes(json_dict)
    else:
        raise exception.NotSupportedDataClass(data_type=data_type)


def instance_from_dict(json_dict):
    pass


def instance_to_dict(dataclass_instance):
    dataclass_dict = dataclass_instance.to_dict()
    update_dict = {'uuid': dataclass_instance.uuid,
                   'created_at': dataclass_instance.created_at,
                   'updated_at': dataclass_instance.updated_at,
                   'deleted_at': dataclass_instance.deleted_at}
    dataclass_dict.update(update_dict)
    return dataclass_dict


class DataClass(ABC):

    @abstractmethod
    def create(self):
        raise NotImplementedError()

    @classmethod
    @abstractmethod
    def read(self):
        raise NotImplementedError()

    @abstractmethod
    def update(self):
        raise NotImplementedError()

    @abstractmethod
    def delete(self):
        raise NotImplementedError()

    @classmethod
    @abstractmethod
    def get_list(self):
        raise NotImplementedError()

    @abstractmethod
    def to_dict(self):
        raise NotImplementedError()

    def _get_uuid(self, json_dict):
        if json_dict.get('uuid'):
            return json_dict.get('uuid')
        else:
            return self._generate_uuid()

    def _generate_uuid(self):
        return str(uuid.uuid4())

    def _get_db_client(self):
        db_client_type = CONF['DB']['db_client']
        if db_client_type == 'file':
            return file_client.FileClient()
        elif db_client_type == 'etcd':
            return etcd_client.EtcdClient()
        else:
            raise exception.NotSupportedDbClient(db_client_type=db_client_type)


class DeviceConfiguration(DataClass, device_configuration.Welcome):

    def __init__(self, json_dict):
        self.created_at = None
        self.updated_at = None
        self.deleted_at = None
        self.uuid = self._get_uuid(json_dict)
        self.db_client = super()._get_db_client()

        welcome = super().from_dict(json_dict)
        super().__init__(
            welcome.type,
            welcome.general,
            welcome.hardware,
            welcome.operating_system,
            welcome.radios,
            welcome.interfaces,
            welcome.dns_search,
            welcome.dns_servers,
            welcome.routes)

    def create(self):
        self.created_at = datetime.datetime.now()
        self.db_client.create(self)

    @classmethod
    def read(self):
        self.db_client.read(self)

    def update(self):
        self.updated_at = datetime.datetime.now()
        self.db_client.update(self)

    def delete(self, force=False):
        self.deleted_at = datetime.datetime.now()
        self.db_client.delete(self, force=force)

    @classmethod
    def get_list(self):
        pass

    def to_dict(self):
        pass


class DeviceMonitoring(DataClass, device_monitoring.Welcome):

    def __init__(self, json_dict, device_configuration_uuid):
        self.created_at = None
        self.updated_at = None
        self.deleted_at = None
        self.uuid = self._get_uuid(json_dict)
        self.db_client = super()._get_db_client()

        self.device_configuration_uuid = device_configuration_uuid

        welcome = super().from_dict(json_dict)
        super().__init__(
            welcome.type,
            welcome.general,
            welcome.resources,
            welcome.interfaces)

    def create(self):
        pass

    @classmethod
    def read(self):
        pass

    def update(self):
        pass

    def delete(self):
        pass

    @classmethod
    def get_list(self):
        pass

    def to_dict(self):
        pass


class NetworkCollection(DataClass, network_collection.Welcome):

    def __init__(self, json_dict):
        self.created_at = None
        self.updated_at = None
        self.deleted_at = None
        self.uuid = self._get_uuid(json_dict)
        self.db_client = super()._get_db_client()

        welcome = super().from_dict(json_dict)
        super().__init__(
            welcome.type,
            welcome.collection)

    def create(self):
        pass

    @classmethod
    def read(self):
        pass

    def update(self):
        pass

    def delete(self):
        pass

    @classmethod
    def get_list(self):
        pass

    def to_dict(self):
        pass


class NetworkGraph(DataClass, network_graph.Welcome):

    def __init__(self, json_dict):
        self.created_at = None
        self.updated_at = None
        self.deleted_at = None
        self.uuid = self._get_uuid(json_dict)
        self.db_client = super()._get_db_client()

        welcome = super().from_dict(json_dict)
        super().__init__(
            welcome.type,
            welcome.protocol,
            welcome.version,
            welcome.revision,
            welcome.metric,
            welcome.router_id,
            welcome.topology_id,
            welcome.label,
            welcome.nodes,
            welcome.links)

    def create(self):
        pass

    @classmethod
    def read(self):
        pass

    def update(self):
        pass

    def delete(self):
        pass

    @classmethod
    def get_list(self):
        pass

    def to_dict(self):
        pass


class NetworkRoutes(DataClass, network_routes.Welcome):

    def __init__(self, json_dict, device_configuration_uuid):
        self.created_at = None
        self.updated_at = None
        self.deleted_at = None
        self.uuid = self._get_uuid(json_dict)
        self.db_client = super()._get_db_client()

        self.device_configuration_uuid = device_configuration_uuid

        welcome = super().from_dict(json_dict)
        super().__init__(
            welcome.type,
            welcome.protocol,
            welcome.version,
            welcome.revision,
            welcome.metric,
            welcome.router_id,
            welcome.routes)

    def create(self):
        pass

    @classmethod
    def read(self):
        pass

    def update(self):
        pass

    def delete(self):
        pass

    @classmethod
    def get_list(self):
        pass

    def to_dict(self):
        pass
