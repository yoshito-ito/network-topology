from dataclasses import dataclass

from network_topology.db.dataclass.physical.building import Port
from network_topology.db.dataclass.abstract_dataclass import DataClassBase
import network_topology.db.dataclass.utils as utils
from network_topology.exception import NotSupportedOperation, PortNotFound


@dataclass
class PortLink(DataClassBase):
    first_uuid: str
    second_uuid: str

    @classmethod
    def from_dict(cls, obj: dict):
        assert isinstance(obj, dict)

        uuid = super()._get_uuid(obj)
        type = cls._get_type()
        created_at, updated_at, deleted_at = super()._get_timestamp(obj)

        first_uuid = utils.from_str(obj.get("first_uuid"))
        second_uuid = utils.from_str(obj.get("second_uuid"))

        cls._check_port_exists(first_uuid)
        cls._check_port_exists(second_uuid)

        return cls(uuid, type, created_at, updated_at, deleted_at,
                   first_uuid, second_uuid)

    def to_dict(self) -> dict:
        result: dict = {}
        result['first_uuid'] = utils.from_str(self.first_uuid)
        result['second_uuid'] = utils.from_str(self.second_uuid)
        result = super()._to_dict(result)
        return result

    @staticmethod
    def _get_type():
        return 'PortLink'

    def update(self):
        raise NotSupportedOperation(data_type=self.type,
                                    method="update()")

    # TODO(yoshito-ito): This method should be more general
    # in DataClassBase like: DataClassBase.check_data_exists()
    @staticmethod
    def _check_port_exists(uuid):
        port = Port.read(uuid)
        if port is None:
            raise PortNotFound(uuid=uuid)
