# This code is auto-generated by https://app.quicktype.io/ from
# https://github.com/netjson/netjson/blob/master/examples/network-collection.json

# To use this code, make sure you
#
#     import json
#
# and then, to convert JSON from a string, do
#
#     result = welcome_from_dict(json.loads(json_string))

from dataclasses import dataclass
from typing import Optional, Any, List, TypeVar, Type, cast, Callable


T = TypeVar("T")


def from_int(x: Any) -> int:
    assert isinstance(x, int) and not isinstance(x, bool)
    return x


def from_none(x: Any) -> Any:
    assert x is None
    return x


def from_union(fs, x):
    for f in fs:
        try:
            return f(x)
        except:
            pass
    assert False


def from_float(x: Any) -> float:
    assert isinstance(x, (float, int)) and not isinstance(x, bool)
    return float(x)


def from_str(x: Any) -> str:
    assert isinstance(x, str)
    return x


def to_float(x: Any) -> float:
    assert isinstance(x, float)
    return x


def to_class(c: Type[T], x: Any) -> dict:
    assert isinstance(x, c)
    return cast(Any, x).to_dict()


def from_list(f: Callable[[Any], T], x: Any) -> List[T]:
    assert isinstance(x, list)
    return [f(y) for y in x]


@dataclass
class Properties:
    # lq: Optional[int] = None
    lq: Optional[float] = None
    nlq: Optional[float] = None
    properties_in: Optional[int] = None
    in_txt: Optional[str] = None

    @staticmethod
    def from_dict(obj: Any) -> 'Properties':
        assert isinstance(obj, dict)
        # lq = from_union([from_int, from_none], obj.get("lq"))
        lq = from_union([from_float, from_none], obj.get("lq"))
        nlq = from_union([from_float, from_none], obj.get("nlq"))
        properties_in = from_union([from_int, from_none], obj.get("in"))
        in_txt = from_union([from_str, from_none], obj.get("in_txt"))
        return Properties(lq, nlq, properties_in, in_txt)

    def to_dict(self) -> dict:
        result: dict = {}
        # result["lq"] = from_union([from_int, from_none], self.lq)
        result["lq"] = from_union([from_float, from_none], self.lq)
        result["nlq"] = from_union([to_float, from_none], self.nlq)
        result["in"] = from_union([from_int, from_none], self.properties_in)
        result["in_txt"] = from_union([from_str, from_none], self.in_txt)
        return result


@dataclass
class Link:
    source: str
    target: str
    # cost: int
    cost: float
    properties: Properties
    cost_text: Optional[str] = None

    @staticmethod
    def from_dict(obj: Any) -> 'Link':
        assert isinstance(obj, dict)
        source = from_str(obj.get("source"))
        target = from_str(obj.get("target"))
        # cost = from_int(obj.get("cost"))
        cost = from_float(obj.get("cost"))
        properties = Properties.from_dict(obj.get("properties"))
        cost_text = from_union([from_str, from_none], obj.get("cost_text"))
        return Link(source, target, cost, properties, cost_text)

    def to_dict(self) -> dict:
        result: dict = {}
        result["source"] = from_str(self.source)
        result["target"] = from_str(self.target)
        # result["cost"] = from_int(self.cost)
        result["cost"] = from_float(self.cost)
        result["properties"] = to_class(Properties, self.properties)
        result["cost_text"] = from_union([from_str, from_none], self.cost_text)
        return result


@dataclass
class Node:
    id: str

    @staticmethod
    def from_dict(obj: Any) -> 'Node':
        assert isinstance(obj, dict)
        id = from_str(obj.get("id"))
        return Node(id)

    def to_dict(self) -> dict:
        result: dict = {}
        result["id"] = from_str(self.id)
        return result


@dataclass
class Collection:
    type: str
    protocol: str
    version: str
    revision: str
    metric: str
    router_id: str
    nodes: List[Node]
    links: List[Link]

    @staticmethod
    def from_dict(obj: Any) -> 'Collection':
        assert isinstance(obj, dict)
        type = from_str(obj.get("type"))
        protocol = from_str(obj.get("protocol"))
        version = from_str(obj.get("version"))
        revision = from_str(obj.get("revision"))
        metric = from_str(obj.get("metric"))
        router_id = from_str(obj.get("router_id"))
        nodes = from_list(Node.from_dict, obj.get("nodes"))
        links = from_list(Link.from_dict, obj.get("links"))
        return Collection(type, protocol, version, revision, metric, router_id, nodes, links)

    def to_dict(self) -> dict:
        result: dict = {}
        result["type"] = from_str(self.type)
        result["protocol"] = from_str(self.protocol)
        result["version"] = from_str(self.version)
        result["revision"] = from_str(self.revision)
        result["metric"] = from_str(self.metric)
        result["router_id"] = from_str(self.router_id)
        result["nodes"] = from_list(lambda x: to_class(Node, x), self.nodes)
        result["links"] = from_list(lambda x: to_class(Link, x), self.links)
        return result


@dataclass
class Welcome:
    type: str
    collection: List[Collection]

    @staticmethod
    def from_dict(obj: Any) -> 'Welcome':
        assert isinstance(obj, dict)
        type = from_str(obj.get("type"))
        collection = from_list(Collection.from_dict, obj.get("collection"))
        return Welcome(type, collection)

    def to_dict(self) -> dict:
        result: dict = {}
        result["type"] = from_str(self.type)
        result["collection"] = from_list(lambda x: to_class(Collection, x), self.collection)
        return result


def welcome_from_dict(s: Any) -> Welcome:
    return Welcome.from_dict(s)


def welcome_to_dict(x: Welcome) -> Any:
    return to_class(Welcome, x)
