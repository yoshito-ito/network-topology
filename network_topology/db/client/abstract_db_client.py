from abc import ABC, abstractmethod

import network_topology.conf as conf
import network_topology.db.client.file as file_client
import network_topology.db.client.etcd as etcd_client
import network_topology.exception as exception


CONF = conf.conf()
DB_BACKEND = ('file', 'etcd')


def get_db_client(db_client_type=None):
    if not db_client_type:
        db_client_type = CONF['DB']['db_client']

    if db_client_type == 'file':
        return file_client.FileClient()
    elif db_client_type == 'etcd':
        return etcd_client.EtcdClient()
    else:
        raise exception.NotSupportedDbClient(db_client_type=db_client_type)


class AbstractDbClient(ABC):

    @abstractmethod
    def create(self, obj):
        raise NotImplementedError()

    @abstractmethod
    def read(self, type='', uuid=''):
        raise NotImplementedError()

    @abstractmethod
    def update(self, obj):
        raise NotImplementedError()

    @abstractmethod
    def delete(self, obj):
        raise NotImplementedError()

    @abstractmethod
    def destroy(self, obj):
        raise NotImplementedError()

    @abstractmethod
    def get_list(self):
        raise NotImplementedError()
