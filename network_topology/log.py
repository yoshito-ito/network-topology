from logging import getLogger, FileHandler, DEBUG, INFO, WARNING, ERROR

import network_topology.conf as conf


LOG_LEVELS = {
    'DEBUG': DEBUG,
    'INFO': INFO,
    'WARNING': WARNING,
    'ERROR': ERROR
}

CONF = conf.conf()
LOG_PATH = CONF['Log']['log_path']
LOG_LEVEL = CONF['Log']['level']


def get_logger(modname=__name__):
    log_level = LOG_LEVELS.get(LOG_LEVEL)

    logger = getLogger(__name__)
    logger.setLevel(log_level)

    file_handler = FileHandler(LOG_PATH)
    file_handler.setLevel(log_level)
    logger.addHandler(file_handler)

    return logger
