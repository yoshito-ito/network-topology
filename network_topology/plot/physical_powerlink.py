import networkx as nx

from network_topology.db.dataclass.physical.powerlink import PowerLink
from network_topology.db.dataclass.physical.building import Chassis, Socket
from network_topology.db.dataclass.physical.building import Outlet, Breaker
from network_topology.db.dataclass.physical.building import SwitchBoard
from network_topology.plot.abstract_plot import PlotBase


class PlotPhysicalPowerLink(PlotBase):
    def __init__(self, layout='spring_layout'):
        super().__init__()

        edge_powerlink_list, chassis_list, socket_list = self.\
            _get_edge_powerlink_list()
        self.chassis_list = chassis_list
        self.socket_list = socket_list

        edge_switchboard_list, edge_breaker_list, \
            edge_outlet_list, switchboard_list = self.\
            _get_edge_switchboard_list()
        self.switchboard_list = switchboard_list

        self._add_edges_from(edge_switchboard_list, weight=100)
        self._add_edges_from(edge_breaker_list, weight=100)
        self._add_edges_from(edge_outlet_list, weight=100)
        self._add_edges_from(edge_powerlink_list, weight=1)

        if layout == 'bipartite_layout':
            self.pos = nx.bipartite_layout(self.graph)
        elif layout == 'circular_layout':
            self.pos = nx.circular_layout(self.graph)
        elif layout == 'kamada_kawai_layout':
            self.pos = nx.kamada_kawai_layout(self.graph)
        elif layout == 'planar_layout':
            self.pos = nx.planar_layout(self.graph)
        elif layout == 'random_layout':
            self.pos = nx.random_layout(self.graph)
        elif layout == 'rescale_layout':
            self.pos = nx.rescale_layout(self.graph)
        elif layout == 'rescale_layout_dict':
            self.pos = nx.rescale_layout_dict(self.graph)
        elif layout == 'shell_layout':
            self.pos = nx.shell_layout(self.graph)
        elif layout == 'spring_layout':
            self.pos = nx.spring_layout(self.graph, k=0.8)
        elif layout == 'spectral_layout':
            self.pos = nx.spectral_layout(self.graph)
        elif layout == 'spiral_layout':
            self.pos = nx.spiral_layout(self.graph)
        elif layout == 'multipartite_layout':
            self.pos = nx.multipartite_layout(self.graph)

    def create(self):
        edge_trace = super()._create_edge_trace()
        node_trace = super()._create_node_trace()

        node_trace = self._edit_node_trace(node_trace)

        fig = super()._create_fig(edge_trace, node_trace)

        return fig

    @staticmethod
    def _get_edge_powerlink_list():
        edge_powerlink_list = []
        powerlink_list = PowerLink.get_list()

        chassis_list = Chassis.get_list()
        socket_list = Socket.get_list()

        _chassis_uuid_list = []
        _socket_uuid_list = []
        for powerlink in powerlink_list:
            edge_powerlink = (powerlink.chassis_uuid, powerlink.socket_uuid)
            edge_powerlink_list.append(edge_powerlink)

            _chassis_uuid_list.append(powerlink.chassis_uuid)
            _socket_uuid_list.append(powerlink.socket_uuid)

        # Return only List[Chassis] in PowerLink
        out_chassis_list = []
        for chassis in chassis_list:
            if chassis.uuid in _chassis_uuid_list:
                out_chassis_list.append(chassis)

        # Return only List[Socket] in PowerLink
        out_socket_list = []
        for socket in socket_list:
            if socket.uuid in _socket_uuid_list:
                out_socket_list.append(socket)

        return edge_powerlink_list, out_chassis_list, out_socket_list

    def _get_edge_switchboard_list(self):
        edge_switchboard_list = []
        edge_breaker_list = []
        edge_outlet_list = []
        switchboard_list = SwitchBoard.get_list()

        for socket in self.socket_list:
            edge_outlet = (socket.uuid, socket.parent_uuid)
            edge_outlet_list.append(edge_outlet)

            outlet = Outlet.read(socket.parent_uuid)
            edge_breaker = (outlet.uuid, outlet.parent_uuid)
            edge_breaker_list.append(edge_breaker)

            breaker = Breaker.read(outlet.parent_uuid)
            edge_switchboard = (breaker.uuid, breaker.parent_uuid)
            edge_switchboard_list.append(edge_switchboard)

        return edge_switchboard_list, edge_breaker_list, \
            edge_outlet_list, switchboard_list

    def _edit_node_trace(self, node_trace):
        node_text = []
        node_marder_size = []
        for node in self.graph.nodes():
            notyet = True
            for chassis in self.chassis_list:
                if chassis.uuid == str(node):
                    node_text.append(
                        'Chassis {}: '.format(chassis.name) + str(node))
                    node_marder_size.append(18)
                    notyet = False
                    break
            if notyet:
                for socket in self.socket_list:
                    if socket.uuid == str(node):
                        node_text.append(
                            'Socket {}: '.format(str(socket.number))
                            + str(node))
                        node_marder_size.append(10)
                        notyet = False
                        break
            if notyet:
                for outlet in Outlet.get_list():
                    if outlet.uuid == str(node):
                        node_text.append(
                            'Outlet {}: '.format(str(outlet.name))
                            + str(node))
                        node_marder_size.append(14)
                        notyet = False
                        break
            if notyet:
                for breaker in Breaker.get_list():
                    if breaker.uuid == str(node):
                        node_text.append(
                            'Breaker {}: '.format(str(breaker.name))
                            + str(node))
                        node_marder_size.append(16)
                        notyet = False
                        break
            if notyet:
                for switchboard in SwitchBoard.get_list():
                    if switchboard.uuid == str(node):
                        node_text.append(
                            'SwitchBoard {}: '.format(str(switchboard.name))
                            + str(node))
                        node_marder_size.append(18)
                        notyet = False
                        break

        node_trace.text = node_text
        node_trace.marker.size = node_marder_size
        return node_trace
