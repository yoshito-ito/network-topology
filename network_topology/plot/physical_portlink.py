import networkx as nx

from network_topology.db.dataclass.physical.portlink import PortLink
from network_topology.db.dataclass.physical.building import Chassis
from network_topology.db.dataclass.physical.building import Port
from network_topology.plot.abstract_plot import PlotBase


class PlotPhysicalPortLink(PlotBase):

    def __init__(self):
        super().__init__()

        edge_chassis_list, chassis_list = self._get_edge_chassis_list()
        self.chassis_list = chassis_list
        edge_port_list, portlink_list, port_list = self._get_edge_port_list()
        self.portlink_list = portlink_list
        self.port_list = port_list

        super()._add_edges_from(edge_chassis_list, weight=100)
        super()._add_edges_from(edge_port_list, weight=1)

        self.pos = nx.spring_layout(self.graph, k=0.3)

    def create(self):
        edge_trace = super()._create_edge_trace()
        node_trace = super()._create_node_trace()

        node_trace = self._edit_node_trace(node_trace)

        fig = super()._create_fig(edge_trace, node_trace)

        # if super()._is_env_notebook():
        #    super()._iplot(fig)

        return fig

    @staticmethod
    def _get_edge_port_list():
        edge_port_list = []
        portlink_list = PortLink.get_list()
        port_list = Port.get_list()
        _port_uuid_list = []
        for portlink in portlink_list:
            edge_port = (portlink.first_uuid, portlink.second_uuid)
            edge_port_list.append(edge_port)

            _port_uuid_list.append(portlink.first_uuid)
            _port_uuid_list.append(portlink.second_uuid)

        # Return only List[Port] in PortLink
        for port in port_list:
            if port.uuid not in _port_uuid_list:
                port_list.remove(port)

        return edge_port_list, portlink_list, port_list

    @staticmethod
    def _get_edge_chassis_list():
        edge_chassis_list = []
        chassis_list = Chassis.get_list()
        for chassis in chassis_list:
            # When chassis has Ports directly
            if chassis.ports:
                for port in chassis.ports:
                    edge_chassis = (chassis.uuid, port.uuid)
                    edge_chassis_list.append(edge_chassis)
            # When chassis has Slot/Module/Port,
            # now skipping Slot and Module for simple structure
            if chassis.slots:
                for slot in chassis.slots:
                    for module in slot.modules:
                        for port in module.ports:
                            edge_chassis = (chassis.uuid, port.uuid)
                            edge_chassis_list.append(edge_chassis)
        return edge_chassis_list, chassis_list

    def _edit_node_trace(self, node_trace):
        node_text = []
        node_marder_size = []
        for node in self.graph.nodes():
            node_is_port = True
            for chassis in self.chassis_list:
                if chassis.uuid == str(node):
                    node_text.append(
                        'Chassis {}: '.format(chassis.name) + str(node))
                    node_marder_size.append(18)
                    node_is_port = False
            if node_is_port:
                for port in self.port_list:
                    if port.uuid == str(node):
                        node_text.append(
                            'Port {}: '.format(port.name) + str(node))
                        node_marder_size.append(10)

        node_trace.text = node_text
        node_trace.marker.size = node_marder_size
        return node_trace
